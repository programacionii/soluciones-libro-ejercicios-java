/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1;

import es.ujaen.prog2.solucioneslibroejerciciosjava.Entrada;
import java.time.LocalDate;

/**
 *
 * @author admin
 */
public class Bol1Ejercicio14 {

    public static void ejecutar() {

        //Pedir nota 0-10 y pasar a suspenso, aprobado, etc...
        boolean continuar = true;

        do {

            System.out.println("Introduce una nota numérica (0-10): ");
            float nota = (float) Entrada.real();

            if (nota >= 0 && nota <= 10) {

                if (nota < 5.0) {
                    System.out.println("Suspenso");
                } else if (nota < 7.0) {
                    System.out.println("Aprobado");
                } else if (nota < 9.0) {
                    System.out.println("Notable");
                } else {
                    System.out.println("Sobresaliente");
                }

            } else {
                System.out.println("Nota incorrecta.");
                System.out.println("Adios.");
                continuar = false;

            }

        } while (continuar);

    }

}
