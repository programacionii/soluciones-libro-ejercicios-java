/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1;

import es.ujaen.prog2.solucioneslibroejerciciosjava.Entrada;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Bol1Ejercicio2 {
    
    //Pedir el radio de un círculo y calcular su área. A=PI*r^2.
    
    public static void ejecutar(){
    
        double radio;
        
        System.out.println("Boletín 1 - Ejercicio 2");
        System.out.println("Introduce el radio:");
        radio = Entrada.real();
        
        double area = Math.PI * Math.pow(radio, radio);
        
        System.out.println("El área es " + area);
    
    }
    
}
