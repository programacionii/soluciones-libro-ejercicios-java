/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1;

import es.ujaen.prog2.solucioneslibroejerciciosjava.Entrada;

/**
 *
 * @author admin
 */
public class Bol1Ejercicio12 {

    public static void ejecutar() {

        //Pedir un número entre 0 y 9.999 y mostrarlo con las cifras al revés.
        System.out.println("Introduce un número entre 0 y 9.999:");
        int numero = Entrada.entero();

        if (numero >= 0 && numero <= 9999) {
            
            String numeroCadena = String.valueOf(numero);

            for (int i = numeroCadena.length() - 1; i >= 0; i--) {

                System.out.print(numeroCadena.charAt(i));

            }

        } else {

            System.out.println("Número fuera de rango.");

        }

    }

}
