/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava.boletin3;

import es.ujaen.prog2.solucioneslibroejerciciosjava.Entrada;

/**
 *
 * @author admin
 */
public class Bol3Ejercicio4 {

    public static void ejecutar() {

        System.out.println("Introduce un entero positivo: ");
        int n = Entrada.entero();

        for (int i = 0; i < n; i++) {
            
            for (int j = 0; j < n; j++) {

                System.out.print("*");

            }
            System.out.println();
        }

    }

}
