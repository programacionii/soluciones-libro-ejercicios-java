/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava;

import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1.Bol1Ejercicio12;
import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1.Bol1Ejercicio14;
import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin1.Bol1Ejercicio2;
import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin2.Bol2Ejercicio7;
import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin3.Bol3Ejercicio1;
import es.ujaen.prog2.solucioneslibroejerciciosjava.boletin3.Bol3Ejercicio4;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {

    public static void main(String[] args) {

//        Bol1Ejercicio2.ejecutar();
//        Bol1Ejercicio12.ejecutar();
//        Bol1Ejercicio14.ejecutar();
//        Bol2Ejercicio7.ejecutar();
//        Bol3Ejercicio1.ejecutar();
        Bol3Ejercicio4.ejecutar();

    }

}
