/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucioneslibroejerciciosjava.boletin2;

import es.ujaen.prog2.solucioneslibroejerciciosjava.Entrada;

/**
 *
 * @author admin
 */
public class Bol2Ejercicio7 {

    public static void ejecutar() {

        //Pedir números hasta que se introduzca uno negativo, y calcular la media.
        boolean continuar = true;
        int sumatoria = 0;
        int contador = 0;

        do {

            System.out.println("Introduce número positivo: ");
            int numero = Entrada.entero();

            if (numero >= 0) {

                contador++;
                sumatoria += numero;

            } else {
                
                if (contador == 0) {
                    System.out.println("No se han introducido números.");
                } else {
                    float media = 1.0f * sumatoria / contador;

                    System.out.println("La media es: " + media);

                }

                continuar = false;

            }

        } while (continuar);

    }

}
